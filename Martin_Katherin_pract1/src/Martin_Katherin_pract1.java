import modelo.Calendario;
import modelo.Medicamento;
import modelo.Paciente;
/**
 * 
 * @author Katherin Martin
 *
 */
public class Martin_Katherin_pract1 {

	static Paciente[] auxPaciente;
	static Medicamento[] auxMedicament;
	Paciente pac = new Paciente();
	Medicamento med = new Medicamento();

	public static void main(String[] args) {
		Martin_Katherin_pract1 inicio = new Martin_Katherin_pract1();
		inicio.cargadeDatosMedicacion();
		inicio.cargadeDatosPaciente();
		inicio.agregaMedicamentoaPaciente();
		inicio.pruebaFunciones();
		

	}

	/**
	 * Datos de prueba para cargar datos de pacientes y medicamentos
	 */

	public void cargadeDatosPaciente() {
		Paciente p1 = new Paciente("Olga", "Palau Yonng");
		Paciente p2 = new Paciente("Jaume", "Tips Llull");
		Paciente p3 = new Paciente("Denis", "Martin Cipriano");
		Paciente p4 = new Paciente("Dean", "Winchester Novak");
		Paciente p5 = new Paciente("Meg", "Novak Demions");

		p1.addcodiTargeta();
		p2.addcodiTargeta();
		p3.addcodiTargeta();
		p4.addcodiTargeta();
		p5.addcodiTargeta();

		auxPaciente = new Paciente[] { p1, p2, p3, p4, p5 };
		// imprime los pacientes creados
		/*
		 * System.out.println("Pacientes:"); for (int i = 0; i < auxPaciente.length;
		 * i++) { System.out.println(auxPaciente[i]); * }
		 */
	}

	public void cargadeDatosMedicacion() {
		Medicamento m1 = new Medicamento("1.Furosemida cinfa 40mg");
		Medicamento m2 = new Medicamento("2.Amoxicilina 500mg");
		Medicamento m3 = new Medicamento("3.Almax Forte\t");
		Medicamento m4 = new Medicamento("4.Paracetamol 1000 mg");
		Medicamento m5 = new Medicamento("5.Aspirina 250 mg\t");
		Medicamento m6 = new Medicamento("6.Bisolvon Forte\t");

		m1.setDuracion(12);
		m2.setDuracion(8);
		m3.setDuracion(12);
		m4.setDuracion(45);
		m5.setDuracion(8);
		m6.setDuracion(6);

		m1.setDosis(1);
		m1.setUnitats("ut");

		m2.setDosis(1);
		m2.setUnitats("ut");

		m3.setDosis(200);
		m3.setUnitats("ml");

		m4.setDosis(1);
		m4.setUnitats("ut");

		m5.setDosis(1);
		m5.setUnitats("ut");

		m6.setDosis(200);
		m6.setUnitats("ml");

		m1.setFrequencia(24);
		m2.setFrequencia(8);
		m3.setFrequencia(24);
		m4.setFrequencia(12);
		m5.setFrequencia(8);
		m6.setFrequencia(12);

		m1.setPauta('n', 'n', 'y');
		m2.setPauta('y', 'y', 'y');
		m3.setPauta('n', 'y', 'n');
		m4.setPauta('y', 'n', 'y');
		m5.setPauta('y', 'y', 'y');
		m6.setPauta('n', 'y', 'y');
		auxMedicament = new Medicamento[] { m1, m2, m3, m4, m5, m6 };
		// Imprime los medicamentos creados

		/*
		 * System.out.println(
		 * "Medicamentos:\nNom\t\t\tDuracio\tDosis\tFrequencia\tPauta"); for (int i = 0;
		 * i < auxMedicament.length; i++) { System.out.println(auxMedicament[i]); }
		 */
	}

	/**
	 * Asigna medicamentos a diferentes pacientes
	 */
	public void agregaMedicamentoaPaciente() {

		Calendario c1 = new Calendario();
		c1.crearFecha(10, 9, 2020);

		Calendario c2 = new Calendario();
		c2.crearFecha(1, 9, 2020);

		Calendario c3 = new Calendario();
		c3.crearFecha(13, 10, 2020);
		System.out.println("CodiTargeta\tNom i Congnom\t\tFecha\t\tMedicacion\t\tDuracion Dosis\tFrecuencia Pauta");
		auxPaciente[0].agregarMedicamento(auxMedicament[0]);
		auxPaciente[0].setIniciTratamiento(c1);

		auxPaciente[0].agregarMedicamento(auxMedicament[5]);
		auxPaciente[0].setIniciTratamiento(c1);
		auxPaciente[0].printmedicament();

		auxPaciente[1].agregarMedicamento(auxMedicament[1]);
		auxPaciente[1].setIniciTratamiento(c2);

		auxPaciente[1].agregarMedicamento(auxMedicament[3]);
		auxPaciente[1].setIniciTratamiento(c1);
		auxPaciente[1].printmedicament();

		auxPaciente[2].agregarMedicamento(auxMedicament[2]);
		auxPaciente[2].setIniciTratamiento(c3);
		auxPaciente[2].printmedicament();

		auxPaciente[3].agregarMedicamento(auxMedicament[3]);
		auxPaciente[3].setIniciTratamiento(c3);

		auxPaciente[4].agregarMedicamento(auxMedicament[4]);
		auxPaciente[4].setIniciTratamiento(c2);
		auxPaciente[4].printmedicament();

		auxPaciente[3].agregarMedicamento(auxMedicament[0]);
		auxPaciente[3].setIniciTratamiento(c3);
		auxPaciente[3].printmedicament();

	}

	/**
	 * 1r cantidad del total de medicina que consumira la final del tratamiento 2n
	 * comparacion de 2 tratamientos
	 */
	public void pruebaFunciones() {
		/*-------------------------------------1r-Cantidad total del tratamiento---------------------------------------------------------------------*/
		int cantidad;
		cantidad = auxPaciente[0].quantitatMedicamentFinal(auxMedicament[0]);

		if (cantidad < 0) {
			System.out.println(auxPaciente[0].getNom() + " no toma el medicamento " + auxMedicament[0]);
		} else {
			System.out.println("\n\n" + auxPaciente[0].getNom() + " habra consumido un total de " + cantidad
					+ auxMedicament[0].getUnitats() + " al finalizar su tratamiento con " + auxMedicament[0].getNom());
		}

		cantidad = auxPaciente[1].quantitatMedicamentFinal(auxMedicament[3]);

		if (cantidad < 0) {
			System.out.println(auxPaciente[1].getNom() + " no toma el medicamento " + auxMedicament[3]);
		} else {
			System.out.println(auxPaciente[1].getNom() + " habra consumido un total de " + cantidad
					+ auxMedicament[3].getUnitats() + "  al finalizar su tratamiento con " + auxMedicament[3].getNom());
		}

		cantidad = auxPaciente[2].quantitatMedicamentFinal(auxMedicament[2]);

		if (cantidad < 0) {
			System.out.println(auxPaciente[2].getNom() + " no toma el medicamento " + auxMedicament[2]);
		} else {
			System.out.println(auxPaciente[2].getNom() + " habra consumido un total de " + cantidad
					+ auxMedicament[2].getUnitats() + " al finalizar su tratamiento con " + auxMedicament[2].getNom());
		}

		/*---------------------------------------2n-Tratamiento mas duracion----------------------------------------------------------------------*/

		med = pac.comparacionTratamientoDuracion(auxPaciente[2], auxPaciente[3]);
		if (med != null) {
			System.out.println("\n\nEl mayor tratamiento es con un duracion de " + med.getDuracion() + " dias");

		} else {
			System.out.println("Ambos tratamientos tienen una misma duracion.");
		}
		med = pac.comparacionTratamientoDuracion(auxPaciente[0], auxPaciente[2]);
		if (med != null) {
			System.out.println("El mayor tratamiento es con un duracion de " + med.getDuracion() + " dias");

		} else {
			System.out.println("Ambos tratamientos tienen una misma duracion.");
		}

		/*-----------------------------------------3r- Es corta duracion-----------------------------------------------------*/
		System.out.println();
		/*Cambia la duracion a 11*/
		auxMedicament[0].setNumCortaduracion(11);
		for (int i = 0; i < auxMedicament.length; i++) {
			System.out.println("Corta duracion:" + auxMedicament[i].esCortaDurada() +" --duracion Medicamento: "+auxMedicament[i].getDuracion());
		}


	}

}
