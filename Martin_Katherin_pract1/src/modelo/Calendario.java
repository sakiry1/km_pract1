package modelo;

import java.util.Calendar;

/**
 * Representa la clase Calendario para las fechas
 * @author Katherin Martin
 *
 */
public class Calendario {

	private Calendar c;

	/**
	 * Constructor de la clase Calendario iniciando su instancia al crearlo
	 */
	public Calendario() {
		this.c = Calendar.getInstance();
	}

	public Calendar getC() {
		return c;
	}

	public void setC(Calendar c) {
		this.c = c;
	}

	/**
	 * recibe en numero, que sumara los dias de tratamiento
	 * 
	 * @param d as Integer
	 * @return c as Calendar
	 */
	public Calendar diasTratamiento(int d) {
		getC().set(Calendar.DATE, d);
		return c;
	}

	/**
	 * recibe el dia, mes, a�o por parametro y lo convierte el tipo calendar
	 * 
	 * @param dia  as int
	 * @param mes  as int
	 * @param year as int
	 */
	public void crearFecha(int dia, int mes, int year) {
		c.set(Calendar.YEAR, year);
		c.set(Calendar.MONTH, mes);
		c.set(Calendar.DATE, dia);

	}

	/**
	 * recibe un numero que sumara a la fecha actual
	 * 
	 * @param dia as int
	 */
	public void agregarAfecha(int dia) {
		getC().add(Calendar.DATE, dia);
	}

	@Override
	public String toString() {
		return c.get(Calendar.DATE) + "/" + c.get(Calendar.MONTH) + "/" + c.get(Calendar.YEAR);
	}

}
