package modelo;

import java.util.Arrays;

/**
 * Representa la clase Medicamentos
 * 
 * @author Katherin Martin
 *
 */
public class Medicamento {

	private String nom;
	private int duracion;
	private int dosis;
	private String unitats;
	private int frequencia;
	private char pauta[] = new char[] {};
	private boolean cortaDurada;
	static int numCortaduracion = 30;

	/**
	 * Constructor de la clase Medicamento
	 * 
	 * @param nom as string
	 */
	public Medicamento(String nom) {
		this.nom = nom;
		this.cortaDurada = esCortaDurada();
	}

	public Medicamento() {

	}

	/**
	 * nombre del medicamentoS
	 * 
	 * @return nom as string
	 */
	public String getNom() {
		return nom;
	}

	public char[] getPauta() {
		return pauta;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public int getDosis() {
		return dosis;
	}

	public void setDosis(int dosis) {
		this.dosis = dosis;
	}

	public String getUnitats() {
		return unitats;
	}

	public void setUnitats(String unitats) {
		this.unitats = unitats;
	}

	public void setFrequencia(int frequencia) {
		this.frequencia = frequencia;
	}

	/**
	 * Recibe por par�metro 3 char que representan ma�ana, tarde y noche para asi
	 * formatearlos a char numerico
	 * 
	 * @param mati  as char
	 * @param tarda as char
	 * @param nit   as char
	 */
	public void setPauta(char mati, char tarda, char nit) {
		char opcion;
		char auxchar[] = new char[3];
		int aux = 0;
		if (mati == 'y') {
			opcion = '1';
			auxchar[aux] = opcion;
		} else {
			opcion = '0';
			auxchar[aux] = opcion;
		}
		aux++;
		if (tarda == 'y') {
			opcion = '1';
			auxchar[aux] = opcion;
		} else {
			opcion = '0';
			auxchar[aux] = opcion;
		}
		aux++;
		if (nit == 'y') {
			opcion = '1';
			auxchar[aux] = opcion;
		} else {
			opcion = '0';
			auxchar[aux] = opcion;
		}
		aux++;

		this.pauta = auxchar;
	}

	public int getNumCortaduracion() {
		return numCortaduracion;
	}

	/**
	 * Para cambiar el numero de corta duracion revisa que el numero ingresado este
	 * entre el rango si lo esta cambiara al nuevo valor, si no volvera a su valor
	 * por defectos
	 * 
	 * @param numCortaduracion as integer
	 */
	public void setNumCortaduracion(int numCortaduracion) {
		if (rangoDuracion(numCortaduracion)) {
			Medicamento.numCortaduracion = numCortaduracion;
		}

	}

	/**
	 * Comprueba si es de corta duracion el medicamento
	 * 
	 * @return esCorta as boolean
	 */
	public boolean esCortaDurada() {
		boolean esCorta = false;
		if (getDuracion() <= getNumCortaduracion()) {
			esCorta = true;
		}
		return esCorta;
	}

	/**
	 * Comprueba que el rango ingresado por parametro este entre los rangos deseados
	 * 
	 * @param num as int
	 * @return correcto as boolean
	 */

	public boolean rangoDuracion(int num) {
		boolean correcto = false;
		if (num > 7 && num < 60) {
			correcto = true;
		}
		return correcto;
	}

	@Override
	public String toString() {
		return nom + "\t" + duracion + "\t" + dosis + " " + unitats + "\t" + frequencia + "\t" + Arrays.toString(pauta);
	}

}
