package modelo;

/**
 * Paciente representa la clase principal del proyecto
 * 
 * @author Katherin Martin
 *
 */
public class Paciente {

	private String nom;
	private String apellido;
	private String codiTargeta;
	private Medicamento[] m;
	private Calendario iniciTratamiento;

	/**
	 * Constructor para la clase principal Paciente
	 * 
	 * @param nom      as string
	 * @param apellido as string
	 */

	public Paciente(String nom, String apellido) {
		this.nom = nom;
		this.apellido = apellido;
		m = new Medicamento[] {};
	}

	public Paciente() {
		super();
	}

	/**
	 * nombre del paciente
	 * 
	 * @return nom as string
	 */
	public String getNom() {
		return nom;
	}

	/**
	 * apellidos separados por un espacio
	 * 
	 * @return apellido as string
	 */
	public String getApellido() {
		return apellido;
	}

	/**
	 * crea un numero de 10 digitos, decidi hacerlo con un random para evitar estar
	 * pasando un numero para el metodo addcodiTargeta
	 * 
	 * @return coditargeta as string
	 */
	public String getCodiTargeta() {
		long valor = (long) (Math.random() * 9_000_000_000L) + 1_000_000_000L;
		codiTargeta = String.valueOf(valor);
		return codiTargeta;
	}

	public void setCodiTargeta(String codiTargeta) {
		this.codiTargeta = codiTargeta;
	}

	/**
	 * Llama a la array de medicamentos que pertenece al Paciente
	 * 
	 * @return m as array de Medicamentos
	 */
	public Medicamento[] getM() {
		return m;
	}

	/**
	 * Actualiza la array de medicamentos cuando lo agregamos
	 * 
	 * @param m as array de medicamentos
	 */
	public void setM(Medicamento[] m) {
		this.m = m;

	}

	/**
	 * Llama a la instancia Calendario
	 * 
	 * @return iniciTratamiento as Calendario
	 */
	public Calendario getIniciTratamiento() {
		return iniciTratamiento;
	}

	/**
	 * Agrega o cambia la fecha pasada por introducir
	 * 
	 * @param iniciTratamiento as Calendario
	 */
	public void setIniciTratamiento(Calendario iniciTratamiento) {
		this.iniciTratamiento = iniciTratamiento;
	}

	/**
	 * Asigna el coditargeta con los apellidos y numero que fue creado en
	 * getCodiTargeta
	 */
	public void addcodiTargeta() {
		String[] apellido;
		apellido = getApellido().split(" ");
		String aux = apellido[0].substring(0, 2).concat(apellido[1].substring(0, 2)).concat(getCodiTargeta());
		setCodiTargeta(aux);
	}

	/**
	 * Agrega al paciente los tratamientos que tiene con diferentes medicamentos
	 * 
	 * @param m Medicacion
	 */
	public void agregarMedicamento(Medicamento m) {
		int a = getM().length;
		int b = a + 1;
		Medicamento[] aux = new Medicamento[b];// aumenta en 1 la array para medicamento
		Medicamento[] aux2 = getM(); // guardar la array pasada con los medicamentos

		if (getM().length > 0) {// me dice si hay elementos en la array

			for (int i = 0; i < aux2.length; i++) { // pasa todos los valores a la array nueva
				aux[i] = aux2[i];
			}
			aux[b - 1] = m; // agrega al final el medicamento nuevo

		} else {
			aux[a] = m; // agrega el medicamento 1r vez a la array
		}
		setM(aux);

	}

	/**
	 * Nos mostrara si el paciente tiene asignado ese medicamento pasado por
	 * parametro, si lo tiene nos dara la cantidad total de pastillas o ml que habra
	 * tomado al finalizar el tratamiento Si no lo tiene nos avisara que no tiene
	 * tratamiento con ese medicamento
	 * 
	 * @param m as String
	 * @return Total as integer
	 */
	public int quantitatMedicamentFinal(Medicamento m) {
		Medicamento[] lista = getM();
		int total = 0, contap = 0, cont = 0;
		char[] p;
		boolean estroba = true;
		while (estroba && cont < lista.length) {
			if (lista[cont].getNom().equals(m.getNom())) { // si el medicamento se encuentra en esa lista
				p = lista[cont].getPauta();
				for (int j = 0; j < p.length; j++) {
					if (p[j] == '1') {
						contap++;
					}
				}
				total = lista[cont].getDuracion() * (lista[cont].getDosis() * contap); // nos da la cantidad de
																						// pastillas o ml q toma al dia,
																						// que despues multiplicamos
																						// con la duracion

				estroba = false;

			}
			cont++;
		}
		return total;

	}

	/**
	 * Compara el tratamiento de mas duracion entre 2 pacientes que tienen asignados
	 * sus medicamentos
	 * 
	 * @param p1 Paciente
	 * @param p2 Paciente
	 * @return integer
	 */
	public Medicamento comparacionTratamientoDuracion(Paciente p1, Paciente p2) {

		if (p1.getM()[0].getDuracion() < p2.getM()[0].getDuracion()) {
			return p2.getM()[0];
		} else if (p1.getM()[0].getDuracion() == p2.getM()[0].getDuracion()) {
			return null;
		} else {
			return p1.getM()[0];
		}

	}

	public void comparacionQuantitat(Medicamento p1, Medicamento p2) {

	}

	/**
	 * Impresion como en tabla como la practica
	 */
	public void printmedicament() {
		for (int i = 0; i < m.length; i++) {
			System.out.println(codiTargeta + "\t" + nom + " " + apellido + "\t" + iniciTratamiento + "\t" + m[i]);

		}
	}

	/**
	 * Metodo toString para dar formato visual a los pacientes
	 */
	@Override
	public String toString() {
		return codiTargeta + " " + nom + " " + apellido;
	}

}
