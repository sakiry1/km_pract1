
import java.math.BigInteger;
import java.util.List;

import modelo.CribaErastotenes;
import modelo.Tipo;

public class Primox {
	static Tipo t = new Tipo();

	public static void main(String[] args) {
		Primox pri = new Primox();

		BigInteger bi1 = new BigInteger("92233720368547758074444444344444555555545454545444454444444444444444");
		BigInteger blon = new BigInteger("9223372036854775806");
		//BigInteger blon = new BigInteger("9223372036854775259")
		BigInteger bint = new BigInteger("2147483647");
		BigInteger opc1 = new BigInteger("1000175014");

		long l1 = 435277881435277881L;
		long l2 = 435277342817854L;
		long l3 = 43527788123L;
		long l4 = 9223372036854775807L;// max long

		int in1 = 542563;
		int in2 = 967545512;
		int inm = 2147483647;// max int

		int respuesta = t.comparador(bint);
		muestraUltimoPrimo(bint, respuesta);

		respuesta = t.comparador(opc1);
		muestraUltimoPrimo(opc1, respuesta);

		//respuesta = t.comparador(blon);
	//	muestraUltimoPrimo(blon, respuesta);
		
		CribaErastotenes ce= new CribaErastotenes();
		
		List<Integer> primes =ce.criba(1000000);
		for (int i = 0; i < primes.size(); i++) {
			System.out.println(primes.get(i));
		}


	}

	private static void muestraUltimoPrimo(BigInteger numero, int opcion) {
		boolean esprimo = false;
		switch (opcion) {
		case -1:
			int cambint = numero.intValue();
			while (esprimo == false) {
				esprimo = t.esPrimoint(cambint);
				if (esprimo) {
					System.out.println("ultimo primo-->" + cambint);
				}
				cambint--;
			}
			esprimo = false;
			break;
		case 0:
			long cambiolong = numero.longValue();

			while (esprimo == false) {
				esprimo = t.esPrimolong(cambiolong);
				if (esprimo) {
					System.out.println("ultimo primo " + cambiolong);
				}
				cambiolong--;
			}
			break;
		case 1:
			while (esprimo == false) {
				numero = numero.subtract(BigInteger.ONE);
				esprimo = t.esPrimoBI(numero);
				if (esprimo) {
					System.out.println("ultimo primo " + numero);
				}
			}
			break;
		default:
			System.out.println("Error de opcion.");
			break;
		}
		System.out.println();
	}
}
