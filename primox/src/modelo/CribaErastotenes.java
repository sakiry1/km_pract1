package modelo;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CribaErastotenes {

	public static List<Integer> criba(int n) {
		final boolean primo[] = new boolean[n + 1];
		Arrays.fill(primo, true);

		for (int p = 2; p * p <= n; p++) {
			if (primo[p]) {
				
				for (int i = p * 2; i <= n; i += p)
					primo[i] = false;
			}
		}

		final List<Integer> primos = new LinkedList<>();
		for (int i = 2; i <= n; i++) {
			if (primo[i])
				primos.add(i);
		}
		return primos;
	}


	
}
