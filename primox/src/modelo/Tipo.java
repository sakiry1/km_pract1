package modelo;

import java.math.BigInteger;

public class Tipo {

	private int intMAX = Integer.MAX_VALUE; // 2^32 -1
	private long logMAX = Long.MAX_VALUE; // 2^64 -1

	public Tipo() {
		super();
	}

	public int comparador(BigInteger b) {

		int comparar = b.compareTo(BigInteger.valueOf(logMAX));
		if (comparar < 0) { // es long o int
			comparar = b.compareTo(BigInteger.valueOf(intMAX));
			if (comparar < 0) {
				System.out.println("Es Integer #"+b);
				return -1;

			} else {
				System.out.println("Es long #"+b);
				return 0;
			}
		} else {
			System.out.println("Es BigInteger #"+b);
			return 1;
		}
	}

	public boolean esPrimoint(int num) {
		int cont = 2;
		boolean esprimo = true;
		while (esprimo && cont < (int) Math.sqrt(num)) {
			if (num % cont == 0) {
				esprimo = false;
			}
			cont++;
		}
		return esprimo;
	}

	public boolean esPrimolong(long num) {
		int cont = 2;
	//	System.out.println("num-->"+num);
		boolean esprimo = true;
		while (esprimo && cont < (long) Math.sqrt(num)) {
			if (num % cont == 0) {
				esprimo = false;
			}
			cont++;
		//	System.out.println("ini "+cont+" max "+(long) Math.sqrt(num));

		}
		return esprimo;
	}

	public boolean esPrimoBI(BigInteger b) {
		boolean esPrimo = true;
		BigInteger aux = BigInteger.TWO;
		BigInteger aux2;

		while (esPrimo && aux.compareTo(b.sqrt()) == -1) {
			aux2 = b.mod(aux);
			if (aux2 == BigInteger.ZERO) {
				esPrimo = false;
			}
			aux = aux.add(BigInteger.ONE);
		}
		return esPrimo;
	}
}
